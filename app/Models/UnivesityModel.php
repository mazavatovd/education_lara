<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UnivesityModel extends Model
{
    use HasFactory;

    protected $table = 'university';
}
