<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TrainingModel extends Model
{
    use HasFactory;

    protected $table = 'training';

    public static function copyFromLastYear($year, $uid)
    {
        $data = DB::table('training')
            ->where('year', (int)$year - 1)
            ->where('user_id', $uid)
            ->get();

        if (empty($data->toArray())) {
            return redirect()->back()->with('error', 'В прошлом году не было повышений!');
        }

        foreach ($data as $datum) {
            DB::table('training')
                ->where('year', $year)
                ->where('user_id', $uid)
                ->insert([
                    'year' => $year,
                    'user_id' => $uid,
                    'qualification_exists' => $datum->qualification_exists,
                    'qualification_plan' => $datum->qualification_plan,
                    'internship_exists' => $datum->internship_exists,
                    'internship_plan' => $datum->internship_plan,
                ]);
        }
    }
}
