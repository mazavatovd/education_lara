<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RoleModel extends Model
{
    use HasFactory;

    protected $table = 'roles';

    public $role_name;

    public $view_users;

    public $edit_users;

    public $edit_iprp;

    public function GetPermition($value)
    {
        $users_roles = new UserRoleModel();
        $user_role = $users_roles->getTable();

        return DB::table($this->table)
            ->join($user_role, $this->table . '.id', '=', $user_role . '.role_id')
            ->where($user_role . '.user_id', auth()->user()->id)
            ->value($this->table . '.' . $value);
    }

    public static function store(array $data)
    {
        foreach ($data as $datum) {
            $role = new RoleModel();

            $role->role_name = $datum['role_name'];
            $role->view_users = $datum['view_users'] ?? null;
            $role->edit_users = $datum['edit_users'] ?? null;
            $role->edit_iprp = $datum['edit_iprp'] ?? null;

            $role->save();
        }
    }
}
