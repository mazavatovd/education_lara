<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserRoleModel extends Model
{
    use HasFactory;

    protected $table = 'users_roles';

    protected $fillable =  [
      'user_id',
    ];
}
