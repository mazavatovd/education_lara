<?php

namespace App\Http\Abstracts;

use App\Models\RoleModel;
use App\Models\TrainingModel;
use App\Models\User;
use Illuminate\Support\Facades\DB;

abstract class OperationsWithTraining
{
    public static function getTrainingUserData($user_id, $page, $redirect_back = false)
    {
        $user = new User();

        $data['year'] =  DB::table('training')
            ->select('year')
            ->where('user_id', $user_id)
            ->limit(1)
            ->value('year');

        $data['main'] = DB::table('training')
            ->select('*')
            ->where('user_id', $user_id)
            ->where('year', $data['year'])
            ->get()
            ->toArray();

        $user_data = $user
            ->where('id', $user_id)
            ->first();

        if (!empty($data['main'])) {
            $data['pager'] = self::getTrainingYear($user_id);
            $data['user_data'] = $user_data;
        }

        return ($redirect_back && !$data['main']) ? redirect()->back()->with('error', 'У данного пользователя отсутствуют повышения!') : view($page)->with('data', $data);
    }

    private static function getTrainingYear($user_id)
    {
        return DB::table('training')
            ->select('year')
            ->distinct('year')
            ->where('user_id', $user_id)
            ->orderBy('year')
            ->get()
            ->toArray();
    }

    public static function dataTraining($user_id, $request, $page)
    {
        $user = new User();
        if ($request['data'] == 'create') {
            return view('pages.training')->with('data', null);
        } else {
            $data['year'] = $request['data'];

            $data['main'] = DB::table('training')
                ->select('*')
                ->where('user_id', $user_id)
                ->where('year', $request['data'])
                ->get()
                ->toArray();

            $user_data = $user
                ->where('id', $user_id)
                ->first();

            if ($data) {
                $data['pager'] = self::getTrainingYear($user_id);
                $data['user_data'] = $user_data;
            }

            return view($page)->with('data', $data);
        }
    }

    public static function createTraining($user_id, $request)
    {
        $request = $request->validate([
            'year' => 'string|min:4|max:4',
        ]);

        DB::table('training')
            ->insert([
                'year' => $request['year'],
                'user_id' => $user_id
            ]);

        return redirect()->route('training.page');
    }

    public static function updateTraining($request)
    {
        $request_array = [
            'qualification_exists' => $request->input('qualification_exists'),
            'qualification_plan' => $request->input('qualification_plan'),
            'internship_exists' => $request->input('internship_exists'),
            'internship_plan' => $request->input('internship_plan'),
        ];

        $training_id = $request->input('training_id');

        DB::table('training')
            ->where('id', $training_id)
            ->update($request_array);

        return redirect()->back();
    }

    public static function deleteTraining($request)
    {
        $roles = new RoleModel();

        $training = TrainingModel::find($request->input('delete'));

        if ($training->user_id !== auth()->user()->id && !$roles->getPermition('edit_users')) {
            return redirect()->back()->with('error', 'Удаление повышений не доступно для вас!');
        } else {
            $training->delete();
            return redirect()->back()->with('success', 'Повышение успешно удалено!');
        }
    }

    public static function copyTraining($request)
    {
        $roles = new RoleModel();

        if ($request->input('uid') !== auth()->user()->id && !$roles->getPermition('edit_users')) {
            return redirect()->back()->with('error', 'Изменение повышений не доступно для вас!');
        } else {
            TrainingModel::copyFromLastYear($request->input('year'), $request->input('uid'));
            return redirect()->back()->with('success', 'Повышения успешно скопированы!');
        }
    }
}