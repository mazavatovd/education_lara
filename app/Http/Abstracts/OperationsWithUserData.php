<?php

namespace App\Http\Abstracts;

use App\Http\Requests\PersonalBirthDateRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

abstract class OperationsWithUserData
{
    public static function getUserData($user_id, $page)
    {
        $user = new User();

        $data = $user
            ->select('users.id as uid', 'users.*')
            ->where('users.id', $user_id)
            ->first();
        $data['job_title'] = DB::table('job_title')
            ->select('title')
            ->where('id', $data['job_title_id'])
            ->value('title');
        $data['education'] = (array)DB::table('education')
            ->selectRaw('*')
            ->join('university', 'education.university_id', '=', 'university.id')
            ->where('education.user_id', $user_id)
            ->get()
            ->first();
        $data['plan'] = (array)DB::table('plan')
            ->selectRaw('*')
            ->where('user_id', $user_id)
            ->get()
            ->first();
        $data['work'] = (array)DB::table('plan')
            ->selectRaw('*')
            ->where('user_id', $user_id)
            ->get()
            ->first();
        $data['iprp'] = (array)DB::table('iprp')
            ->selectRaw('*')
            ->where('user_id', $user_id)
            ->get()
            ->first();

        $data['user_id'] = $user_id;

        $data = $data ? $data->toArray() : [];

        return view($page)->with('data', $data);
    }

    public static function insertFio($user_id, $data)
    {
        $user = new User();
        $user->where('id', $user_id)
            ->update([
                'name' => $data['name'],
                'surname' => $data['surname'],
                'lastname' => $data['lastname'],
            ]);

        return redirect()->back();
    }

    public static function insetBirthDate($user_id, $birth_date)
    {
        DB::table('users')
            ->where('id', $user_id)
            ->update([
                'birth_date' => $birth_date,
            ]);

        return redirect()->back();
    }

    public static function insetJobTitle($user_id, $data)
    {
        if ($data['action'] == 'update') {
            DB::table('job_title')
                ->where('id', $data['id'])
                ->update([
                    'title' => $data['title'],
                ]);
        } elseif ($data['action'] == 'insert') {
            $id = DB::table('job_title')
                ->insertGetId([
                    'title' => $data['title'],
                ]);

            DB::table('users')
                ->where('id', $user_id)
                ->update([
                    'job_title_id' => $id
                ]);
        }

        return redirect()->back();
    }

    public static function insertEducation($user_id, $data)
    {
        if ($data['action'] == 'update') {
            DB::table('education')
                ->join('university', 'education.university_id', '=', 'university.id')
                ->where('education.id', $data['id'])
                ->update([
                    'university.university_name' => $data['university_name'],
                    'education.year' => $data['year'],
                    'education.speciality' => $data['speciality'],
                ]);
        } elseif ($data['action'] == 'insert') {
            $id = DB::table('university')
                ->insertGetId([
                    'university_name' => $data['university_name'],
                ]);
            DB::table('education')
                ->where('user_id', $user_id)
                ->insert([
                    'year' => $data['year'],
                    'user_id' => $user_id,
                    'university_id' => $id,
                    'speciality' => $data['speciality']
                ]);
        }
        return redirect()->back();
    }

    public static function insertPlan($user_id, $data)
    {
        $request_array = $data;

        $id = DB::table('plan')
            ->select('id')
            ->where('user_id', $user_id)
            ->get()
            ->value('id');

        if ($id) {
            DB::table('plan')
                ->where('id', $id)
                ->update($request_array);

        } else {
            $request_array['user_id'] = $user_id;
            DB::table('plan')
                ->insert(
                    $request_array
                );
        }

        return redirect()->back();
    }

    public static function insertWork($user_id, $data)
    {
        $request_array = $data;

        $id = DB::table('plan')
            ->select('id')
            ->where('user_id', $user_id)
            ->get()
            ->value('id');

        if ($id) {
            DB::table('plan')
                ->where('id', $id)
                ->update($request_array);
        } else {
            $request_array['user_id'] = $user_id;
            DB::table('plan')
                ->insert($request_array);
        }

        return redirect()->back();
    }
}