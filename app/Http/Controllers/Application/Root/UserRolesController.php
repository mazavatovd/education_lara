<?php

namespace App\Http\Controllers\Application\Root;

use App\Http\Controllers\Controller;
use App\Models\RoleModel;
use App\Models\UserRoleModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserRolesController extends Controller
{
    public function GetRoleList(Request $request)
    {
        $role_model = new RoleModel();
        $roles = DB::table('roles')
            ->select('role_name', 'id')
            ->get();

        $roles_list = [];
        foreach ($roles as $role) {
            $roles_list[] = [
                'id' => $role->id,
                'name' => $role->role_name
            ];
        }

        return json_encode($roles_list);
    }

    public function SetRole(Request $request)
    {
        $users_roles = new UserRoleModel();

        $user = $users_roles->where('user_id', $request->input('id_user'))->first();

        if (isset($user->role_id) && $user->role_id == $request->input('id_role')) {
            return redirect()->back()->with('error', 'Данный пользователь и так имеет данную роль!');
        }

        $users_roles
            ->where('user_id', $request->input('id_user'))
            ->update([
            'role_id' => $request->input('id_role')
        ]);

        return redirect()->back()->with('success', 'Роль успешно обновлена!');
    }
}
