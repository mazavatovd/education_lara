<?php

namespace App\Http\Controllers\Application\Root;

use App\Http\Abstracts\OperationsWithTraining;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserTrainingController extends Controller
{
    public function index(Request $request)
    {
        $user_id = $request->input('uid');

        return OperationsWithTraining::getTrainingUserData($user_id, 'pages.root.user_training', true);
    }

    public function data(Request $request)
    {
        $user_id = $request->input('uid');

        return OperationsWithTraining::dataTraining($user_id, $request, 'pages.root.user_training');
    }

    public function create(Request $request)
    {
        $user_id = $request->input('uid');;

        return OperationsWithTraining::createTraining($user_id, $request);
    }

    public function update(Request $request)
    {
        return OperationsWithTraining::updateTraining($request);
    }

    public function delete(Request $request)
    {
        return OperationsWithTraining::deleteTraining($request);
    }

    public function copy(Request $request)
    {
        return OperationsWithTraining::copyTraining($request);
    }
}
