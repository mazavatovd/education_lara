<?php

namespace App\Http\Controllers\Application\Root;

use App\Http\Abstracts\OperationsWithUserData;
use App\Http\Controllers\Controller;
use App\Http\Requests\PersonalBirthDateRequest;
use Illuminate\Http\Request;

class UserDataController extends Controller
{
    public function index(Request $request)
    {
        $user_id = $request['uid'];
        $page = 'pages.root.user_data';
        return OperationsWithUserData::getUserData($user_id, $page);
    }

    public function insertFio(Request $request)
    {
        $user_id = $request->input('user_id');
        $data['name'] = $request->input('name');
        $data['surname'] = $request->input('surname');
        $data['lastname'] = $request->input('lastname');

        return OperationsWithUserData::insertFio($user_id, $data);
    }

    public function insetBirthDate(PersonalBirthDateRequest $request)
    {
        $user_id = $request->input('user_id');
        $birth_date = $request->input('birth_date');

        return OperationsWithUserData::insetBirthDate($user_id, $birth_date);
    }

    public function insetJobTitle(Request $request)
    {
        $user_id = $request->input('user_id');
        $data['action'] = $request->input('action');
        $data['id'] = $request->input('id');
        $data['title'] = $request->input('title');

        return OperationsWithUserData::insetJobTitle($user_id, $data);
    }

    public function insertEducation(Request $request)
    {
        $user_id = $request->input('user_id');
        $data['action'] = $request->input('action');
        $data['id'] = $request->input('id');
        $data['year'] = $request->input('year');
        $data['speciality'] = $request->input('speciality');
        $data['university_name'] = $request->input('university_name');

        return OperationsWithUserData::insertEducation($user_id, $data);
    }

    public function insertPlan(Request $request)
    {
        $data = [
            'work_year' => $request->input('work_year'),
            'discipline' => $request->input('discipline'),
            'category' => $request->input('category'),
            'category_date' => $request->input('category_date'),
            'experience' => $request->input('experience'),
            'training_place' => $request->input('training_place'),
            'training_date' => $request->input('training_date'),
            'degree' => $request->input('degree'),
            'degree_year' => $request->input('degree_date')
        ];

        $user_id = $request->input('user_id');

        return OperationsWithUserData::insertPlan($user_id, $data);
    }

    public function insertWork(Request $request)
    {
        $user_id = $request->input('user_id');

        $data = [
            'prepare_ymk' => $request->input('prepare_ymk'),
            'introduction_of_effective_teaching_methods' => $request->input('introduction_of_effective_teaching_methods'),
            'interdisciplinary_connections' => $request->input('interdisciplinary_connections'),
            'independent_work' => $request->input('independent_work'),
            'open_events' => $request->input('open_events'),
            'extracurricular_activities' => $request->input('extracurricular_activities'),
            'educational_literature' => $request->input('educational_literature'),
            'new_forms' => $request->input('new_forms'),
            'prepare_materials' => $request->input('prepare_materials'),
            'education_process' => $request->input('education_process'),
            'activity' => $request->input('activity'),
            'articles' => $request->input('articles'),
            'achievements' => $request->input('achievements')
        ];

        return OperationsWithUserData::insertWork($user_id, $data);
    }
}
