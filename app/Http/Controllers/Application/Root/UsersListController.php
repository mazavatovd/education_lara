<?php

namespace App\Http\Controllers\Application\Root;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UsersListController extends Controller
{
    public function index()
    {
        $users = User::all();

        return view('pages.root.users_list')->with('data', $users);
    }
}
