<?php

namespace App\Http\Controllers\Application\Root;

use App\Http\Controllers\Controller;
use App\Models\IprpModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserHeaderController extends Controller
{
    public function index(Request $request)
    {
        $user_id = $request['uid'];
        $iprp = new IprpModel();
        $user = new User();

        $data = $iprp
            ->where('user_id', $user_id)
            ->first();

        $user_data = $user
            ->where('id', $user_id)
            ->first();

        $data = $data ? $data->toArray() : [];

        $data['action'] = $data ? 'update' : 'create';
        $data['uid'] = $user_id;
        $data['user_data'] = $user_data;

        return view('pages.root.user_header')->with('data', $data);
    }

    public function createIprp(Request $request)
    {
        $user_id = $request->input('uid');
        $request_array = [
            'year' => $request->input('year'),
            'CMC' => $request->input('CMC'),
            'teacher_name' => $request->input('teacher_name'),
            'discipline' => $request->input('discipline'),
            'speciality' => $request->input('speciality')
        ];

        $id = DB::table('iprp')
            ->select('id')
            ->where('user_id', $user_id)
            ->get()
            ->value('id');

        if ($id) {
            DB::table('iprp')
                ->where('id', $id)
                ->update($request_array);
        } else {
            $request_array['user_id'] = $user_id;
            DB::table('iprp')
                ->insert($request_array);
        }

        return redirect()->back();
    }
}
