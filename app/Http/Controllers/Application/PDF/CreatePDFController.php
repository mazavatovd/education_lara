<?php

namespace App\Http\Controllers\Application\PDF;

use App\Http\Controllers\Controller;
use App\Models\JobTitleModel;
use App\Models\RoleModel;
use App\Models\TrainingModel;
use App\Models\UnivesityModel;
use App\Models\User;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CreatePDFController extends Controller
{
    public function action(Request $request)
    {
        $roles = new RoleModel();

        if ((int)$request['uid'] !== auth()->user()->id && !$roles->getPermition('view_users')) {
            return redirect()->back()->with('error', 'Печать других пользователей не доступна для вас!');
        }

        $data = self::GetData($request['uid']);

        $pdf_data = [
            'title' => 'Данные преподавателя ' .
                ($data['users']->surname . ' ' .
                $data['users']->name . ' ' .
                $data['users']->lastname) ?? '',
            'iprp_CMC' => $data['iprp']->CMC ?? '',
            'iprp_year' => $data['iprp']->year ?? '',
            'iprp_teacher_name' => $data['iprp']->teacher_name ?? '',
            'iprp_discipline' => $data['iprp']->discipline ?? '',
            'iprp_speciality' => $data['iprp']->speciality ?? '',
            'name' => $data['users']->name ?? '',
            'surname' => $data['users']->surname ?? '',
            'lastname' => $data['users']->lastname ?? '',
            'birth_date' => date('d.m.Y', strtotime($data['users']->birth_date)) ?? '',
            'job_title' => $data['job_title']->title ?? '',
            'university_name' => $data['university']->university_name ?? '',
            'education_year' => isset($data['education']->year) ? date('d.m.Y', strtotime($data['education']->year)) : '',
            'work_year' => $data['plan']->work_year ?? '',
            'plan_discipline' => $data['plan']->discipline ?? '',
            'plan_category' => $data['plan']->category ?? '',
            'category_date' =>  isset($data['plan']->category_date) ? date('d.m.Y', strtotime($data['plan']->category_date)) : '',
            'experience' => $data['plan']->experience ?? '',
            'training_place' => $data['plan']->training_place ?? '',
            'training_date' => isset($data['plan']->training_date) ? date('d.m.Y', strtotime($data['plan']->training_date)) : '',
            'degree' => $data['plan']->degree ?? '',
            'degree_year' => $data['plan']->degree_year ?? '',
            'hours_1' => $data['plan']->hours_1 ?? '',
            'hours_2' => $data['plan']->hours_2 ?? '',
            'prepare_ymk' => $data['plan']->prepare_ymk ?? '',
            'introduction_of_effective_teaching_methods' => $data['plan']->introduction_of_effective_teaching_methods ?? '',
            'interdisciplinary_connections' => $data['plan']->interdisciplinary_connections ?? '',
            'independent_work' => $data['plan']->independent_work ?? '',
            'open_events' => $data['plan']->open_events ?? '',
            'extracurricular_activities' => $data['plan']->extracurricular_activities ?? '',
            'educational_literature' => $data['plan']->educational_literature ?? '',
            'new_forms' => $data['plan']->new_forms ?? '',
            'prepare_materials' => $data['plan']->prepare_materials ?? '',
            'education_process' => $data['plan']->education_process ?? '',
            'activity' => $data['plan']->activity ?? '',
            'articles' => $data['plan']->articles ?? '',
            'achievements' => $data['plan']->achievements ?? '',
            'training' => $data['training'],
        ];

        $pdf = PDF::loadView('files.pdf', $pdf_data);

        return $pdf->stream();

    }

    private static function GetData($uid): array|null
    {
        $fields = [
            'iprp', 'education',
            'plan',
        ];

        foreach ($fields as $field) {
            $data[$field] = DB::table($field)
                ->where('user_id', $uid)
                ->first();
        }
        $data['users'] = User::select('*')->where('id', $uid)->first();
        $data['job_title'] = isset($data['users']->job_title_id) ? JobTitleModel::select('*')->where('id', $data['users']->job_title_id)->first() : '';
        $data['university'] = isset($data['education']->university_id) ?  UnivesityModel::select('*')->where('id', $data['education']->university_id)->first() : '';
        $data['training'] = TrainingModel::select('*')
            ->where('user_id', $uid)
            ->orderBy('year')
            ->get();

        return $data;
    }
}
