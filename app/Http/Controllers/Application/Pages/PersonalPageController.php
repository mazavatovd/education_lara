<?php

namespace App\Http\Controllers\Application\Pages;

use App\Http\Abstracts\OperationsWithUserData;
use App\Http\Controllers\Controller;
use App\Http\Requests\PersonalBirthDateRequest;
use App\Models\JobTitleModel;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function PHPUnit\Framework\containsIdentical;


class PersonalPageController extends Controller
{

    public function index()
    {
        $user_id = auth()->user()->id;
        $page = 'pages.personal';

        return OperationsWithUserData::getUserData($user_id, $page);
    }

    public function insertFio(Request $request)
    {
        $user_id = auth()->user()->id;
        $data['name'] = $request->input('name');
        $data['surname'] = $request->input('surname');
        $data['lastname'] = $request->input('lastname');

        return OperationsWithUserData::insertFio($user_id, $data);
    }

    public function insetBirthDate(PersonalBirthDateRequest $request)
    {
        $user_id = auth()->user()->id;
        $birth_date = $request->input('birth_date');

        return OperationsWithUserData::insetBirthDate($user_id, $birth_date);
    }

    public function insetJobTitle(Request $request)
    {
        $user_id = auth()->user()->id;
        $data['action'] = $request->input('action');
        $data['id'] = $request->input('id');
        $data['title'] = $request->input('title');

        return OperationsWithUserData::insetJobTitle($user_id, $data);
    }

    public function insertEducation(Request $request)
    {
        $user_id = auth()->user()->id;
        $data['action'] = $request->input('action');
        $data['id'] = $request->input('id');
        $data['year'] = $request->input('year');
        $data['speciality'] = $request->input('speciality');
        $data['university_name'] = $request->input('university_name');

        return OperationsWithUserData::insertEducation($user_id, $data);
    }

    public function insertPlan(Request $request)
    {
        $data = [
            'work_year' => $request->input('work_year'),
            'discipline' => $request->input('discipline'),
            'category' => $request->input('category'),
            'category_date' => $request->input('category_date'),
            'experience' => $request->input('experience'),
            'training_place' => $request->input('training_place'),
            'training_date' => $request->input('training_date'),
            'degree' => $request->input('degree'),
            'degree_year' => $request->input('degree_date'),
            'hours_1' => $request->input('hours_1'),
            'hours_2' => $request->input('hours_2'),
        ];

        $user_id = auth()->user()->id;

        return OperationsWithUserData::insertPlan($user_id, $data);
    }

}
