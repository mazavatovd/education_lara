<?php

namespace App\Http\Controllers\Application\Pages;

use App\Http\Abstracts\OperationsWithUserData;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WorkPageController extends Controller
{
    public function index()
    {
        $data = (array)DB::table('plan')
            ->selectRaw('*')
            ->where('user_id', auth()->user()->id )
            ->get()
            ->first();

        return view('pages.work')->with('data', $data);
    }

    public function insertWork(Request $request)
    {
        $user_id = auth()->user()->id;

        $data = [
            'prepare_ymk' => $request->input('prepare_ymk'),
            'introduction_of_effective_teaching_methods' => $request->input('introduction_of_effective_teaching_methods'),
            'interdisciplinary_connections' => $request->input('interdisciplinary_connections'),
            'independent_work' => $request->input('independent_work'),
            'open_events' => $request->input('open_events'),
            'extracurricular_activities' => $request->input('extracurricular_activities'),
            'educational_literature' => $request->input('educational_literature'),
            'new_forms' => $request->input('new_forms'),
            'prepare_materials' => $request->input('prepare_materials'),
            'education_process' => $request->input('education_process'),
            'activity' => $request->input('activity'),
            'articles' => $request->input('articles'),
            'achievements' => $request->input('achievements')
        ];

        return OperationsWithUserData::insertWork($user_id, $data);
    }
}
