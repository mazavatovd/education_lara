<?php

namespace App\Http\Controllers\Application\Pages;

use App\Http\Abstracts\OperationsWithTraining;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function GuzzleHttp\Promise\all;

class TrainingPageController extends Controller
{
    public function index()
    {
       $user_id = auth()->user()->id;

       return OperationsWithTraining::getTrainingUserData($user_id, 'pages.training');
    }

    public function data(Request $request)
    {
        $user_id = auth()->user()->id;

        return OperationsWithTraining::dataTraining($user_id, $request, 'pages.training');
    }

    public function create(Request $request)
    {
       $user_id = auth()->user()->id;

       return OperationsWithTraining::createTraining($user_id, $request);
    }

    public function update(Request $request)
    {
        return OperationsWithTraining::updateTraining($request);
    }

    public function delete(Request $request)
    {
        return OperationsWithTraining::deleteTraining($request);
    }

    public function copy(Request $request)
    {
        return OperationsWithTraining::copyTraining($request);
    }
}
