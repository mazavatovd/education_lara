<?php

namespace App\Http\Controllers\Application\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\LoginRequest;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use MongoDB\Driver\Session;

class LoginController extends Controller
{
    public function index()
    {
        return view('pages.auth.login');
    }

    public function login(LoginRequest $request)
    {
        $user = User::whereEmail($request->input('email'))->first();

        if ($user) {
            if ($user->password == $request->input('password')) {
                auth()->login($user);

                return redirect()->route('personal.page');
            }
        }

        return redirect()->back()->withErrors(['auth_error' => 'Логин/Пароль введены неверно!']);
    }

    public function logout()
    {
        auth()->logout();
        return redirect()->route('login.form');
    }
}
