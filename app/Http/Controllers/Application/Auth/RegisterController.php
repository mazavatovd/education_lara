<?php

namespace App\Http\Controllers\Application\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\RegisterRequest;
use App\Models\User;
use App\Models\UserRoleModel;

class RegisterController extends Controller
{
    public function index()
    {
        return view('pages.auth.register');
    }

    public function register(RegisterRequest $request)
    {
        $user = User::create($request->validated());

        UserRoleModel::create([
            'user_id' => $user->id
        ]);

        return redirect()->back()->with('success', 'Регистрация прошла успешно!');
    }
}
