<?php

namespace App\Http\Middleware\Roles;

use App\Models\RoleModel;
use App\Models\UserRoleModel;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ViewUsersMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $role_model = new RoleModel();

        if (!$role_model->GetPermition('view_users')) {
            return redirect()->back()->with('error', 'Просмотр данных пользователей недоступно для вашей роли!');
        }
        return $next($request);
    }
}
