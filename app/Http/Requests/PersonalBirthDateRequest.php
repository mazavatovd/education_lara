<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PersonalBirthDateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'birth_date' => ['required', 'date']
        ];
    }
}
