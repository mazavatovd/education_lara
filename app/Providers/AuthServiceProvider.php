<?php

namespace App\Providers;

use App\Models\RoleModel;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Blade::if('admin', function () {
            $role_model = new RoleModel();

            if ($role_model->GetPermition('is_admin')) {
                return true;
            }
        });

        Blade::if('view_users', function () {
            $role_model = new RoleModel();
            $permition = $role_model->GetPermition('view_users');

            if ($permition) {
                return true;
            }
        });

        Blade::if('edit_users', function () {
            $role_model = new RoleModel();
            $permition = $role_model->GetPermition('edit_users');

            if ($permition) {
                return true;
            }
        });
    }
}
