$(window).on('load', function () {
    const el = $('[id^="switch_role"]');
    const close_role_list = $("#close_role_list");
    $.each(el, function (index, value) {
        $(value).on("click", function () {
            const this_id = $(this).prop('id');
            const last = $(this).prop('id').toString().slice(-1);
            $("#roles_field" + last).toggleClass("container mt-2 border")
            $.ajax({
                url: '/roles',
                method: 'get',
                dataType: 'json',
                success: function (data) {
                    $(data).each(function () {
                        const id = this.id;
                        const name = this.name;

                        $("#roles_field" + last).append(function () {
                            return $(
                                '<form action="/roles" method="post">\n' +
                                '<input type="hidden" name="_token" value="' + $('meta[name="csrf-token"]').attr('content') + '">\n' +
                                '<input type="hidden" name="id_role" value="' + id + '">\n' +
                                '<input type="hidden" name="id_user" value="' + last + '">\n' +
                                '<button type="submit" class="btn btn-outline-dark text-lg-start float-start m-2">' + name + '</button>' +
                                '</form>'
                            )
                        });
                    });

                    $("#roles_field" + last).attr("id", "used");
                    $("#" + this_id).attr("id", "close_role_list");
                },
                error: function (data) {
                    console.log(data.error);
                }
            });
        });
    });
});
