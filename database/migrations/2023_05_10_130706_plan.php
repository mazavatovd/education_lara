<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plan', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->integer('work_year')->nullable();
            $table->text('discipline')->nullable();
            $table->text('category')->nullable();
            $table->date('category_date')->nullable();
            $table->text('experience')->nullable();
            $table->text('training_place')->nullable();
            $table->date('training_date')->nullable();
            $table->text('degree')->nullable();
            $table->integer('degree_year')->nullable();
            $table->text('activity')->nullable();
            $table->text('prepare_ymk')->nullable();
            $table->text('introduction_of_effective_teaching_methods')->nullable();
            $table->text('interdisciplinary_connections')->nullable();
            $table->text('independent_work')->nullable();
            $table->text('open_events')->nullable();
            $table->text('extracurricular_activities')->nullable();
            $table->text('educational_literature')->nullable();
            $table->text('new_forms')->nullable();
            $table->text('prepare_materials')->nullable();
            $table->text('education_process')->nullable();
            $table->text('articles')->nullable();
            $table->text('achievements')->nullable();
            $table->integer('hours_1')->nullable();
            $table->integer('hours_2')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('plan');
    }
};
