<?php

namespace Database\Seeders;

use App\Models\RoleModel;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'surname' => 'нет',
            'name' => 'Администратор',
            'lastname' => 'нет',
            'email' => 'admin@admin.adm',
            'password' => 'Pa$$w0rd!',
        ]);

        DB::table('roles')
            ->insert([
                'role_name' => 'Администратор',
                'view_users' => true,
                'edit_users' => true,
                'edit_iprp' => true,
                'is_admin' => true,
            ]);
        DB::table('roles')
            ->insert([
                'role_name' => 'ПЦМК',
                'view_users' => true,
                'edit_users' => true,
                'edit_iprp' => true
            ]);
        DB::table('roles')
            ->insert([
                'role_name' => 'Методист',
                'view_users' => true,
                'edit_users' => true,
                'edit_iprp' => true
            ]);
        DB::table('roles')
            ->insert([
                'role_name' => 'Заместитель директора',
                'view_users' => true,
                'edit_users' => true,
                'edit_iprp' => true
            ]);
        DB::table('roles')
            ->insert( [
                'role_name' => 'Преподаватель',
            ]);

        DB::table('users_roles')->insert([
            'user_id' => '1',
            'role_id' => '1'
        ]);
    }
}
