<?php

use App\Http\Controllers\Application\Auth\LoginController;
use App\Http\Controllers\Application\Auth\RegisterController;
use App\Http\Controllers\Application\Pages\PersonalPageController;
use App\Http\Controllers\Application\Pages\TrainingPageController;
use App\Http\Controllers\Application\Pages\WorkPageController;
use App\Http\Controllers\Application\Root\UserDataController;
use App\Http\Controllers\Application\Root\UserHeaderController;
use App\Http\Controllers\Application\Root\UserRolesController;
use App\Http\Controllers\Application\Root\UsersListController;
use App\Http\Controllers\Application\Root\UserTrainingController;
use App\Http\Controllers\Application\PDF\CreatePDFController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/personal');

Route::controller(RegisterController::class)->middleware(['auth', 'admin'])->group(function () {
    Route::get('/register', 'index')->name('register.form');
    Route::post('/register', 'register')->name('register.action');
});

Route::controller(LoginController::class)->middleware('guest')->group(function () {
    Route::get('/login', 'index')->name('login.form');
    Route::post('/login', 'login')->name('login.action');
    Route::post('/logout', 'logout')->name('login.logout')->withoutMiddleware('guest');
});

Route::controller(PersonalPageController::class)->middleware('auth')->group(function () {
    Route::get('/personal', 'index')->name('personal.page');
    Route::post('/personal/fio', 'insertFio')->name('personal.fio');
    Route::post('/personal/birth_date', 'insetBirthDate')->name('personal.birth');
    Route::post('/personal/job_title', 'insetJobTitle')->name('personal.job_title');
    Route::post('/personal/education', 'insertEducation')->name('personal.education');
    Route::post('/personal/plan', 'insertPlan')->name('personal.plan');
});

Route::controller(WorkPageController::class)->middleware('auth')->group(function () {
    Route::get('/work', 'index')->name('work.page');
    Route::post('/work/insert', 'insertWork')->name('work.insert');
});

Route::controller(TrainingPageController::class)->middleware('auth')->group(function () {
    Route::post('/training/create', 'create')->name('training.create');
    Route::post('/training/update', 'update')->name('training.update');
    Route::get('/training', 'index')->name('training.page');
    Route::get('/training/{data}', 'data')->name('training.data');
    Route::post('/training/delete', 'delete')->name('training.delete');
    Route::post('/training/copy', 'copy')->name('training.copy');
});

Route::controller(UsersListController::class)->middleware(['auth', 'view_users'])->group(function () {
    Route::get('/users', 'index')->name('users.page');
});

Route::controller(UserHeaderController::class)->middleware(['auth', 'edit_iprp'])->group(function () {
    Route::get('/user/header/{uid}', 'index')->name('user_header.page');
    Route::post('/user/header', 'createIprp')->name('user_header.action');
});

Route::controller(UserTrainingController::class)->middleware(['auth', 'view_users'])->group(function () {
    Route::post('/user/training/create', 'create')->name('user_training.create')->middleware('edit_users');
    Route::post('/user/training/update', 'update')->name('user_training.update')->middleware('edit_users');
    Route::get('/user/training', 'index')->name('user_training.page');
    Route::get('/user/training/{data}', 'data')->name('user_training.data');
    Route::post('/user/training/delete', 'delete')->name('user_training.delete');
    Route::post('/user/training/copy', 'copy')->name('user_training.copy');
});

Route::controller(UserDataController::class)->middleware(['auth', 'view_users'])->group(function () {
    Route::get('/user/{uid}', 'index')->name('user.page');
    Route::post('/user/fio', 'insertFio')->name('user.fio')->middleware('edit_users');
    Route::post('/user/birth_date', 'insetBirthDate')->name('user.birth')->middleware('edit_users');
    Route::post('/user/job_title', 'insetJobTitle')->name('user.job_title')->middleware('edit_users');
    Route::post('/user/education', 'insertEducation')->name('user.education')->middleware('edit_users');
    Route::post('/user/plan', 'insertPlan')->name('user.plan')->middleware('edit_users');
    Route::post('/user/work', 'insertWork')->name('user.work')->middleware('edit_users');
});

Route::controller(UserRolesController::class)->group(function () {
    Route::get('/roles', 'GetRoleList')->name('roles.list');
    Route::post('/roles', 'SetRole')->name('roles.action');
});

Route::controller(CreatePDFController::class)->middleware(['auth'])
    ->group(function () {
    Route::get('/pdf/{uid}', 'action')->name('pdf');
});

