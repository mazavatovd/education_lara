@extends('templates.main')
@section('title')
    Учебно-методическая работа
@endsection

@section('main')
    <h3 class="text-center border border-secondary mt-2 mb-2"><b>Учебно - методическая работа</b></h3>
    @include('templates.forms.work', ['work' => 'work.insert'])
@endsection
