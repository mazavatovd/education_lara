@extends('templates.main')
@section('title')
    Пользовательские данные
@endsection

@section('main')
    <h3 class="text-center border border-secondary mt-2 mb-2">Данные пользователя
        <b>{{ $data['surname'] }} {{ $data['name'] }} {{ $data['lastname'] }}</b></h3>
    @include('templates.forms.fio', ['fio' => 'user.fio', 'birth' => 'user.birth', 'job_title' => 'user.job_title'])
    @include('templates.forms.education', ['education' => 'user.education'])
    @include('templates.forms.plan', ['plan' => 'user.plan'])
    @include('templates.forms.work', ['work' => 'user.work', 'data' => $data['work']])
@endsection