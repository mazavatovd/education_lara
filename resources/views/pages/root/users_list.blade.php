@extends('templates.main')
@section('title')
    Список пользователей
@endsection

@section('main')
    <h3 class="text-center border border-secondary mt-2 mb-2"><b>Список пользователей</b></h3>
    <div class="container text-center mt-4">
        @foreach($data as $user)
            <button class="btn btn-outline-secondary m-2"
                    data-bs-toggle="modal" data-bs-target="#modal_user_{{ $user->id }}" id="user_value">
                <p class="text-dark text-start">
                    <span class="text-dark"><b>ФИО:</b></span>
                    <span id="fio" class="text-dark text-bg-light text-start border border-2">
                        {{ $user->surname }}
                        {{ $user->name }}
                        {{ $user->lastname }}</span>
                    <span class="text-dark m-lg-4"><b>e-mail:</b></span>
                    <span id="p-email"
                          class="text-secondary text-bg-light text-start border border-2">{{ $user->email }}</span>
                </p>
            </button>
            @include('templates.modals.user_action')
        @endforeach
    </div>
@endsection