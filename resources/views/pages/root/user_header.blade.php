@extends('templates.main')
@section('title')
    ИПРП пользователя
@endsection

@section('main')
    <h3 class="text-center border border-secondary mt-2 mb-2">Редактирование ИПРП пользователя <b>{{ $data['user_data']['surname'] }} {{ $data['user_data']['name'] }} {{ $data['user_data']['lastname'] }}</b></h3>
    @include('templates.forms.header', ['route' => 'user_header.action'])
@endsection