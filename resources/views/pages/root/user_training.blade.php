@extends('templates.main')
@section('title')
    Повышения преподавателя
@endsection

@section('main')
    <h3 class="text-center border border-secondary mt-2 mb-2">Повышения квалификации пользователя
        <b>{{ $data['user_data']['surname'] }} {{ $data['user_data']['name'] }} {{ $data['user_data']['lastname'] }}</b>
    </h3>
    @include('templates.forms.training', ['update' => 'user_training.update', 'training_data' => 'user_training.data'])
@endsection