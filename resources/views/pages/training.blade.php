@extends('templates.main')
@section('title')
    Повышение квалификации
@endsection

@section('main')
    <h3 class="text-center border border-secondary mt-2 mb-2"><b>Повышение квалификации</b></h3>
    @if(!empty($data['main']))
        <div class="btn btn-outline-dark display-5 disabled mt-2">
            Год повышения квалификации: <b>{{ $data['year'] ?? '' }}</b>
        </div>
        <div class="d-inline-block mt-2">
            <a class="btn btn-outline-success mt-2"
               style="margin-left: 10px;"
               href="{{ route('training.data', ['data' => 'create']) }}">Добавить повышение</a>
        </div>
        @include('templates.copy_from_previous', ['year' =>  $data['year'] ?? '', 'uid' => $data['user_data']['id']])
        @foreach($data['main'] as $datum)
            <form id="training-form" class="mb-2" action="{{ route('training.update') }}" method="post"
                  enctype="multipart/form-data">
                @csrf
                @if(isset($datum->user_id))
                    <input type="hidden" name="user_id" value="{{ $datum->user_id }}" required>
                @endif
                <input type="hidden" name="training_id" value="{{ $datum->id ?? '' }}" required>
                <table class="table  mt-4" id="table">
                    <thead>
                    <tr>
                        <th scope="col">
                            Примерные формы повышени квалификации
                        </th>
                        <th scope="col">
                            Наличие
                        </th>
                        <th scope="col">
                            План
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            Повышение квалификации
                        </td>
                        <td>
                    <textarea style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " class='form-control' type='text'
                              name='qualification_exists' required>{{ $datum->qualification_exists ?? '' }}</textarea>
                        </td>
                        <td>
                    <textarea style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " class='form-control' type='text'
                              name='qualification_plan' required>{{ $datum->qualification_plan ?? '' }}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Стажировка, аспирантура,<br>
                            курсы, прочее
                        </td>
                        <td>
                    <textarea style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " class='form-control' type='text'
                              name='internship_exists' required>{{ $datum->internship_exists ?? '' }}</textarea>
                        </td>
                        <td>
                    <textarea style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " class='form-control' type='text'
                              name='internship_plan' required>{{ $datum->internship_plan ?? '' }}</textarea>
                        </td>
                    </tr>
                    </tbody>
                </table>
                @include('templates.savebutton')
            </form>
            @include('templates.delbutton', ['training_id' => $datum->id, 'route' => 'training.delete'])
        @endforeach

        <div class="container">
            <nav aria-label="...">
                <ul class="pagination justify-content-center" style="float: right;">
                    @if(isset($data['pager']))
                        @foreach($data['pager'] as $page)
                            @if($page->year == $data['year'])
                                <li class="page-item active" aria-current="page">
                                    <span class="page-link bg-secondary border-secondary">{{ $page->year }}</span>
                                </li>
                            @else
                                <li class="page-item"><a class="page-link text-dark"
                                                         href="{{ route('training.data', ['data' => $page->year]) }}">{{ $page->year }}</a>
                                </li>
                            @endif
                        @endforeach
                    @endif
                </ul>
            </nav>
        </div>
    @else
        <form action="{{ route('training.create') }}"
              method="post" class="text-center" style="margin-top: 150px;">
            @csrf
            @if(isset($data['user_id']))
                <input type="hidden" name="user_id" value="{{ $data['user_id'] }}" required>
            @endif
            <div class="d-inline-block">
                <label class="mb-1 fw-bold" style="font-size: 3ex" for="training">Введите год</label>
                <input type="number" class="form-control" style="font-size: 3ex" name="year" id="training" required>
            </div>
            <div class="d-inline-block m-lg-4">
                <button type="submit" class="btn btn-outline-primary" style="font-size: 3ex; margin-top: -8px">
                    Создать!
                </button>
            </div>
            <div class="position-relative mt-2">
                @if(Session::has('errors'))
                    <button class='btn btn-outline-danger disabled'>{{ Session::get('errors')->first() }}</button>
                @endif
            </div>
        </form>
    @endif
@endsection
