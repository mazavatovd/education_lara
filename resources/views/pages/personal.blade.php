@extends('templates.main')
@section('title')
    Личные данные
@endsection

@section('main')
    @include('templates.printbutton', ['user_id' => auth()->user()->id])
    @include('templates.header')
    <h3 class="text-center border border-secondary mt-2 mb-2"><b>Организационно - педагогическая работа</b></h3>
    @include('templates.forms.fio', ['fio' => 'personal.fio', 'birth' => 'personal.birth', 'job_title' => 'personal.job_title'])
    @include('templates.forms.education', ['education' => 'personal.education'])
    @include('templates.forms.plan', ['plan' => 'personal.plan'])
@endsection
