@extends('templates.main')
@section('title')
    Страница регистрации
@endsection

@section('main')
    <form class="mt-4 mb-5" action="{{ route('register.action') }}" method="post" enctype="multipart/form-data">
        @csrf
        <h3>Форма регистрации</h3>
        <div class="mb-2">Заполните поля для свободной регистрации</div>
        <div class="mb-3">
            <label for="name_input" class="form-label fw-bold">Имя</label>
            <input type="text" name="name" class="form-control" id="name_input" aria-describedby="nameHelp" required>
        </div>
        <div class="mb-3">
            <label for="surname_input" class="form-label fw-bold">Фамилия</label>
            <input type="text" name="surname" class="form-control" id="surname_input" required>
        </div>
        <div class="mb-3">
            <label for="lastname_input" class="form-label fw-bold">Отчество</label>
            <input type="text" name="lastname" class="form-control" id="lastname_input" aria-describedby="loginHelp"
                   required>
        </div>
        <div class="mb-3">
            <label for="login_input" class="form-label fw-bold">Email</label>
            <input type="text" name="email" class="form-control" id="login_input" aria-describedby="loginHelp" required>
        </div>
        <div class="mb-3">
            <label for="password_input" class="form-label fw-bold">Пароль</label>
            <input type="password" name="password" class="form-control" id="password_input" required>
        </div>
        <div class="mb-3">
            <label for="password_confirm" class="form-label fw-bold">Подтвердите пароль</label>
            <input type="password" name="password_confirmation" class="form-control" id="password_confirm" required>
        </div>
        <button type="submit" class="btn btn-primary">Зарегистрироваться</button>
    </form>
@endsection
