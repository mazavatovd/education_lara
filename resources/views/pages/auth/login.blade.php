@extends('templates.main')
@section('title')
    Страница авторизации
@endsection

@section('main')
    <form action="{{ route('login.action') }}" method="post" class="mt-4" enctype="multipart/form-data">
        @csrf
        <h3>Форма входа</h3>
        @error('auth_error')
        <div class="alert alert-danger" role="alert">
            {{ $message }}
        </div>
        @enderror
        <div class="mb-2">Заполните поля для входа на сайт</div>
        <div class="mb-3">
            <label for="login_input" class="form-label fw-bold">E-Mail</label>
            <input type="email" name="email" class="form-control" id="login_input" aria-describedby="loginHelp" required>
        @error('email')
            <div class="invalid-feedback">
                {{ $message }}
            </div>
        @enderror
        </div>
        <div class="mb-3">
            <label for="password_input" class="form-label fw-bold">Пароль</label>
            <input type="password" name="password" class="form-control" id="password_input">
        </div>
        <button type="submit" class="btn btn-outline-primary">Войти</button>
    </form>
@endsection
