<nav class="navbar navbar-expand-lg" id="table">
    <a class="navbar-brand mx-2" href="#">Образование</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    @if(auth()->user())
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">
                <a class="nav-item btn btn-light mx-1" href="{{ route('personal.page') }}">Личные данные</a>
                <a class="nav-item btn btn-light mx-1" href="{{ route('work.page') }}">Учебно-методическая работа</a>
                <a class="nav-item btn btn-light mx-1" href="{{ route('training.page') }}">Повышение квалификации</a>
                @view_users
                    <a class="nav-item btn btn-light mx-1" href="{{ route('users.page') }}">Пользователи</a>
                @endview_users
            </div>
        </div>
    @endif
    <div class="d-flex">
        @if(auth()->guest())
            <a class="btn btn-outline-success" href="{{ route('login.form') }}">Войти</a>
        @else
            @admin
            <a class="btn btn-outline-primary mx-1" href="{{ route('register.form') }}">Регистрация</a>
            @endadmin
            <form action="{{ route('login.logout') }}" method="post">
                @csrf
                <button type="submit" class="btn btn-outline-danger mx-3">Выйти</button>
            </form>
        @endif
    </div>
</nav>
