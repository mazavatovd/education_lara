<form class="d-inline-block mt-2" method="post" action="{{ route('training.copy') }}">
    @csrf
    <input hidden name="year" value="{{ $year }}" required>
    <input hidden name="uid" value="{{ $uid }}" required>
    <button style="margin-left: 10px;" type="submit" class="btn btn-outline-primary mt-2">Скопировать с предыдущего года</button>
</form>