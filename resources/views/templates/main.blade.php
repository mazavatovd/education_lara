<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>
        @yield('title')
    </title>
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <script src="{{ asset('assets/js/jquery-3.7.0.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}" defer></script>
    <script src="{{ asset('assets/js/custom.js') }}" defer></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="bg-light bg-gradient">
<header>
    <div class="container">
        @include('templates.navbar')
    </div>
</header>
<main>
    @if(session()->has('error'))
        <div class="container mt-2" id="error">
            <div class="alert alert-danger" role="alert">
                {{ session()->get('error') }}
            </div>
        </div>
    @elseif(session()->has('success'))
        <div class="container mt-2" id="success">
            <div class="alert alert-success" role="alert">
                {{ session()->get('success') }}
            </div>
        </div>
    @endif
    <div class="container">
        @yield('main')
    </div>
</main>
</body>
</html>

