<form action="{{ route($work) }}" method="post" style="margin-bottom: 10ex" enctype="multipart/form-data">
    @csrf
    @if(isset($data['user_id']))
        <input type="hidden" name="user_id" value="{{ $data['user_id'] }}" required>
    @endif
    <table class="table  mt-4" id="table">
        <thead>
        <tr>
            <th scope="col" style="width: 80ex">
                2.1 Подготовка УМК (в т.ч. изданные в течение отчетного периода КТП,<br>
                рабочая программа, ФОС, методички), соответствие образовательным<br>
                программам, предложение и перспективный план заданий.
            </th>
            <td>
                    <textarea class='form-control' style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " type='text' name='prepare_ymk' required>{{ $data['prepare_ymk'] ?? '' }}</textarea>
            </td>
        </tr>
        <tr>
            <th scope="col" style="width: 80ex">
                2.2 Внедрение эффективных методов преподавания, обобщение,<br>
                обмен педагогическим опытом, использование инновационных технологий,<br>
                взаимопосещения занятий коллег.
            </th>
            <td>
                    <textarea class='form-control' style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " type='text' name='introduction_of_effective_teaching_methods'
                              required>{{ $data['introduction_of_effective_teaching_methods'] ?? '' }}</textarea>
            </td>
        </tr>
        <tr>
            <th scope="col" style="width: 80ex">
                2.3 Междисциплинарные связи.
            </th>
            <td>
                    <textarea class='form-control' style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " type='text' name='interdisciplinary_connections'
                              required>{{ $data['interdisciplinary_connections'] ?? '' }}</textarea>
            </td>
        </tr>
        <tr>
            <th scope="col" style="width: 80ex">
                2.4 Организация самостоятельной работы обучающихся в т.ч. выполнения и<br>
                проверка домашних заданий.
            </th>
            <td>
                    <textarea class='form-control' style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " type='text' name='independent_work'
                              required>{{ $data['independent_work'] ?? '' }}</textarea>
            </td>
        </tr>
        <tr>
            <th scope="col" style="width: 80ex">
                2.5 Проведение открытых мероприятий (темы докладов на заседаниях ЦМК,<br>
                открытых уроков и мероприятий, уроков-экскурсий, прочее).
            </th>
            <td>
                    <textarea class='form-control' style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " type='text' name='open_events'
                              required>{{ $data['open_events'] ?? '' }}</textarea>
            </td>
        </tr>
        <tr>
            <th scope="col" style="width: 80ex">
                2.6 Проведение внеаудиторных мероприятий с обучающимися.
            </th>
            <td>
                    <textarea class='form-control' style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " type='text' name='extracurricular_activities'
                              required>{{ $data['extracurricular_activities'] ?? '' }}</textarea>
            </td>
        </tr>
        <tr>
            <th scope="col" style="width: 80ex">
                2.7 Обеспечение дисциплин учебной литературой.
            </th>
            <td>
                    <textarea class='form-control' style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " type='text' name='educational_literature'
                              required>{{ $data['educational_literature'] ?? '' }}</textarea>
            </td>
        </tr>
        <tr>
            <th scope="col" style="width: 80ex">
                2.8 Внедрения новых форм и методов обучения, средств активизации<br>
                познавательной деятельности обучающихся.
            </th>
            <td>
                   <textarea class='form-control' style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " type='text' name='new_forms'
                             required>{{ $data['new_forms'] ?? '' }}</textarea>
            </td>
        </tr>
        <tr>
            <th scope="col" style="width: 80ex">
                2.9 Подготовка программных материалов для промежуточных аттестаций и<br>
                иных установленных форм контроля (анализ контрольных материалов и<br>
                сессий).
            </th>
            <td>
                    <textarea class='form-control' style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " type='text' name='prepare_materials'
                              required>{{ $data['prepare_materials'] ?? '' }}</textarea>
            </td>
        </tr>
        <tr>
            <th scope="col" style="width: 80ex">
                2.10 Предложения по совершенствованию учебного процесса на<br>
                перспективу.
            </th>
            <td>
                   <textarea class='form-control' style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " type='text' name='education_process'
                             required>{{ $data['education_process'] ?? '' }}</textarea>
            </td>
        </tr>
        <tr>
            <th scope="col" style="width: 80ex">
                2.11 Организация самостоятельной и научно-исследовательской деятельности<br>
                обучающихся (указать мероприятия, например название и место проведение<br>
                олимпиады).
            </th>
            <td>
                    <textarea class='form-control' style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " type='text' name='activity'
                              required>{{ $data['activity'] ?? '' }}</textarea>
            </td>
        </tr>
        <tr>
            <th scope="col" style="width: 80ex">
                2.12 Статьи и публикации в учебном году.
            </th>
            <td>
                    <textarea class='form-control' style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " type='text' name='articles'
                              required>{{ $data['articles'] ?? '' }}</textarea>
            </td>
        </tr>
        <tr>
            <th scope="col" style="width: 80ex">
                2.13 Прочие планируемые достижения в учебном году.
            </th>
            <td>
                   <textarea class='form-control' style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " type='text' name='achievements'
                             required>{{ $data['achievements'] ?? '' }}</textarea>
            </td>
        </tr>
        </thead>
    </table>
    @include('templates.savebutton')
</form>
