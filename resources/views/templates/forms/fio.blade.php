<table class="table" id="table">
    <thead>
    <tr>
        <th scope="col">
            Фамилия, имя, отчество<br>
            преподавателя
        </th>
        <form action="{{ route($fio) }}" method="post" enctype="multipart/form-data">
            @csrf
            @if(isset($data['user_id']))
                <input type="hidden" name="user_id" value="{{ $data['user_id'] }}">
            @endif
            <td>
                <div class="input-group mb-1">
                    <input type="text" class="form-control" placeholder="Фамилия" name="surname"
                           value="@if(isset($data['surname'])) {{ $data['surname'] }} @endif" required>
                    <input type="text" class="form-control" placeholder="Имя" name="name"
                           value="@if(isset($data['name'])) {{ $data['name'] }} @endif" required>
                    <input type="text" class="form-control" placeholder="Отчество" name="lastname"
                           value="@if(isset($data['lastname'])) {{ $data['lastname'] }} @endif" required>
                </div>
            </td>
            @include('templates.savebutton')
        </form>
    </tr>
    <tr>
        <th scope="col">
            Дата рождения
        </th>
        <form action="{{ route($birth) }}" method="post" enctype="multipart/form-data">
            @csrf
            @if(isset($data['user_id']))
                <input type="hidden" name="user_id" value="{{ $data['user_id'] }}" required>
            @endif
            <td>
                <input type="date" class="form-control" name="birth_date" value="{{ $data['birth_date'] ?? '' }}" required>
            </td>
            @include('templates.savebutton')
        </form>
    </tr>
    <tr>
        <th scope="col">
            Все занимаемые должности,<br>
            включая другие организаци<br>
            в текущем году
        </th>
        <form action="{{ route($job_title) }}" method="post" enctype="multipart/form-data">
            @csrf
            @if(isset($data['user_id']))
                <input type="hidden" name="user_id" value="{{ $data['user_id'] }}" required>
            @endif
            <td>
                <div class="form-floating">
                    <input type="hidden" name="action"
                           value="@if(isset($data['job_title'])) update @else insert @endif" required>
                    <input type="hidden" name="id" value="{{ $data['job_title_id'] ?? '' }}" required>
                    <textarea class="form-control" name="title" style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " id="floatingTextarea2" required>{{ $data['job_title'] ?? '' }}</textarea>
                    <label for="floatingTextarea2">Должности:</label>
                </div>
            </td>
            @include('templates.savebutton')
        </form>
    </tr>
    </thead>
</table>
