<form style="margin-bottom: 10ex" action="{{ route($plan) }}" method="post" enctype="multipart/form-data">
    @csrf
    @if(isset($data['user_id']))
        <input type="hidden" name="user_id" value="{{ $data['user_id'] }}" required>
    @endif
    <table class="table  mt-4" id="table">
        <thead>
        <tr>
            <th scope="col">
                Кол-во полных лет работы
            </th>
            <td>
                <div class="input-group mb-1">
                    <input type="number" name="work_year" class="form-control"
                           value="{{ $data['plan']['work_year'] ?? '' }}" required>
                </div>
            </td>
        </tr>
        <tr>
            <th scope="col">
                Преподает дисциплину(ы)
            </th>
            <td>
                <div class="input mb-1">
                    <input type="text" name="discipline" class="form-control" placeholder="Дисциплина"
                           value="{{ $data['plan']['discipline'] ?? '' }}" required>
                </div>
            </td>
        </tr>
        <tr>
            <th scope="col">
                Категория, дата окончания<br>
                категории
            </th>
            <td>
                <div class="input-group mb-1">
                    <input type="text" name="category" value="{{ $data['plan']['category'] ?? '' }}"
                           class="form-control" placeholder="Категория" required>
                    <input type="date" name="category_date" value="{{ $data['plan']['category_date'] ?? '' }}"
                           class="form-control" required>
                </div>
            </td>
        </tr>
        <tr>
            <th scope="col">
                Стаж работы общий/<br>
                педагогический (через дробь)
            </th>
            <td>
                <div class="input-group mb-1">
                    <input type="text" name="experience" class="form-control" placeholder="Стаж"
                           value="{{ $data['plan']['experience'] ?? '' }}" required>
                </div>
            </td>
        </tr>
        <tr>
            <th scope="col">
                Место и время последнего<br>
                повышения квалификации
            </th>
            <td>
                <div class="input-group mb-1">
                    <input type="text" name="training_place" class="form-control"
                           placeholder="Место и время последнего повышения квалификации"
                           value="{{ $data['plan']['training_place'] ?? '' }}" required>
                    <input type="date" name="training_date" class="form-control"
                           placeholder="Место и время последнего повышения квалификации"
                           value="{{ $data['plan']['training_date'] ?? '' }}" required>
                </div>
            </td>
        </tr>
        <tr>
            <th scope="col">
                Звание или ученая степень (год<br>
                получения свидетельства)
            </th>
            <td>
                <div class="input-group mb-1">
                    <input type="text" name="degree" class="form-control"
                           value="{{ $data['plan']['degree'] ?? '' }}" required>
                    <input type="number" name="degree_date" class="form-control"
                           value="{{ $data['plan']['degree_year'] ?? '' }}" required>
                </div>
            </td>
        </tr>
        </thead>
    </table>
    <h5 class="text-center border border-secondary mt-2 mb-2"><b>Количество часов по нагрузке</b></h5>
    <table class="table  mt-4" id="table">
        <tr>
            <th scope="col">
                В первом семестре:
            </th>
            <td>
                <div class="input-group mb-1">
                    <input type="number" name="hours_1" class="form-control"
                           value="{{ $data['plan']['hours_1'] ?? '' }}" required>
                </div>
            </td>
            </tr>
        <tr>
            <th scope="col">
                Во втором семестре:
            </th>
            <td>
                <div class="input-group mb-1">
                    <input type="number" name="hours_2" class="form-control"
                           value="{{ $data['plan']['hours_2'] ?? '' }}" required>
                </div>
            </td>
        </tr>
        </thead>
    </table>
    @include('templates.savebutton')
</form>
