<form action="{{ route($route) }}" method="post" style="margin-bottom: 10ex" enctype="multipart/form-data">
    @csrf
    @if(isset($data['uid']))
        <input type="hidden" name="uid" value="{{ $data['uid'] }}" required>
    @endif
    <input type="hidden" name="action" value="{{ $data['action'] }}" required>
    <table class="table  mt-4" id="table">
        <thead>
        <tr>
            <th scope="col" style="width: 80ex">
                Учебный год ИПРП:
            </th>
            <td>
                    <input class='form-control' type='text' name='year' value="{{ $data['year'] ?? '' }}" required>
            </td>
        </tr>
        <tr>
            <th scope="col" style="width: 80ex">
                Цикловая методическая комиссия:
            </th>
            <td>
                    <textarea class='form-control' style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " type='text' name='CMC'
                              required>{{ $data['CMC'] ?? '' }}</textarea>
            </td>
        </tr>
        <tr>
            <th scope="col" style="width: 80ex">
                Преподаватель:
            </th>
            <td>
                    <input class='form-control' type='text' name='teacher_name' value="{{ $data['teacher_name'] ?? '' }}" required>
            </td>
        </tr>
        <tr>
            <th scope="col" style="width: 80ex">
                Дисциплина, МДК:
            </th>
            <td>
                    <textarea class='form-control' style="
                            height: 5ex;
                            max-height: 20ex;
                            min-height: 5ex;
                            " type='text' name='discipline'
                              required>{{ $data['discipline'] ?? '' }}</textarea>
            </td>
        </tr>
        <tr>
            <th scope="col" style="width: 80ex">
                Специальность:
            </th>
            <td>
                    <input class='form-control' type='text' name='speciality' value="{{ $data['speciality'] ?? '' }}" required>
            </td>
        </tr>
        </thead>
    </table>
    @include('templates.savebutton')
</form>
