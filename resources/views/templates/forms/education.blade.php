<form action="{{ route($education) }}" method="post" enctype="multipart/form-data">
    @csrf
    @if(isset($data['user_id']))
        <input type="hidden" name="user_id" value="{{ $data['user_id'] }}" required>
    @endif
    <table class="table  mt-4" id="table">
        <thead>
        <tr>
            <th scope="col">
                Образование, где и когда<br>
                окончили (название учебного<br>
                заведения полностью)
            </th>
            <td>
                <div class="input-group mb-1">
                    <input type="hidden" name="action"
                           value="{{ isset($data['education']['id']) ? 'update' : 'insert' }}" required>
                    <input type="hidden" name="id" value="{{ $data['education']['id'] ?? '' }}" required>
                    <input type="text" name="university_name" class="form-control" placeholder="Где закончили"
                           value="{{ $data['education']['university_name'] ?? '' }}" required>
                    <input type="date" name="year" class="form-control"
                           value="{{ $data['education']['year'] ?? '' }}" required>
                </div>
            </td>
        </tr>
        <tr>
            <th scope="col">
                Специальность и квалификация<br>
                по диплому
            </th>
            <td>
                <div class="input-group mb-1">
                    <input type="text" name="speciality" class="form-control" placeholder="Специальность"
                           value="{{ $data['education']['speciality'] ?? '' }}" required>
                </div>
            </td>
        </tr>
        </thead>
    </table>
    @include('templates.savebutton')
</form>
