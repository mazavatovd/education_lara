@include('templates.copy_from_previous', ['year' =>  $data['year'] ?? '', 'uid' => $data['user_data']['id']])

@foreach($data['main'] as $datum)
<form id="training-form" action="{{ route($update) }}" method="post" class="mb-2"
      enctype="multipart/form-data">
    @csrf
        @if(isset($datum->user_id))
            <input type="hidden" name="user_id" value="{{ $datum->user_id }}" required>
        @endif
        <input type="hidden" name="training_id" value="{{ $datum->id ?? '' }}" required>
        <table class="table  mt-4" id="table">
            <thead>
            <tr>
                <th scope="col">
                    Примерные формы повышени квалификации
                </th>
                <th scope="col">
                    Наличие
                </th>
                <th scope="col">
                    План
                </th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                    Повышение квалификации
                </td>
                <td>
                    <textarea style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " class='form-control' type='text'
                              name='qualification_exists' required>{{ $datum->qualification_exists ?? '' }}</textarea>
                </td>
                <td>
                    <textarea style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " class='form-control' type='text'
                              name='qualification_plan' required>{{ $datum->qualification_plan ?? '' }}</textarea>
                </td>
            </tr>
            <tr>
                <td>
                    Стажировка, аспирантура,<br>
                    курсы, прочее
                </td>
                <td>
                    <textarea style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " class='form-control' type='text'
                              name='internship_exists' required>{{ $datum->internship_exists ?? '' }}</textarea>
                </td>
                <td>
                    <textarea style="
                            max-height: 20ex;
                            min-height: 10ex;
                            " class='form-control' type='text'
                              name='internship_plan' required>{{ $datum->internship_plan ?? '' }}</textarea>
                </td>
            </tr>
            </tbody>
        </table>
        @include('templates.savebutton')
</form>
@include('templates.delbutton', ['training_id' => $datum->id, 'route' => 'user_training.delete'])
@endforeach
<div class="container">
    <nav aria-label="...">
        <ul class="pagination justify-content-center" style="float: right;">
            @if(isset($data['pager']))
                @foreach($data['pager'] as $page)
                    @if($page->year == $data['year'])
                        <li class="page-item active" aria-current="page">
                            <span class="page-link bg-secondary border-secondary">{{ $page->year }}</span>
                        </li>
                    @else
                        <li class="page-item"><a class="page-link text-dark"
                                                 href="{{ route($training_data, ['data' => $page->year, 'uid' => $data['user_data']['id']]) }}">{{ $page->year }}</a>
                        </li>
                    @endif
                @endforeach
            @endif
        </ul>
    </nav>
</div>
