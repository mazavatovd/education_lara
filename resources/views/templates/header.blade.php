<div class="container text-center mt-1 mb-3 border border-secondary" id="personal-header">
    <div>
        <p class="mb-0">
            Министерство науки и высшего образования Российской Федерации<br>
            федеральное государственное бюджетное образовательное учреждение высшего образования<br>
            <b>"Российский экономический университет им. Г.В. Плеханова"</b><br>
            Среднее профессиональное образование<br>
            <b>Московский приборостроительный техникум</b>
        </p>
        <hr class="mb-0 mt-0">
        <p class="text-secondary">
            <small>(Наименование структурного подразделения СПО)</small>
        </p>
        <p>
            <strong>Индивидуальный план работы преподавателя</strong><br>
            <strong>на {{ $data['iprp']['year'] ?? '' }} учебный год</strong>
        </p>
    </div>
    <div class="container text-lg-start">
        <p>
            Цикловая методическая комиссия: <u>{{ $data['iprp']['CMC'] ?? '' }}</u>
        </p>
        <p>
            Преподаватель: <u>{{ $data['iprp']['teacher_name'] ?? '' }}</u>
        </p>
        <p>
            Дисциплина, МДК: <u>{{ $data['iprp']['discipline'] ?? '' }}</u>
        </p>
        <p>
            Специальность: <u>{{ $data['iprp']['speciality'] ?? '' }}</u>
        </p>
        <hr class="mb-0">
        <p class="text-secondary text-center">
            <small>(перечислить все дисциплины, МДК, практики по каждой специальности, согласно нагрузке)</small>
        </p>
    </div>
</div>
