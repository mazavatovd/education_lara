<form method="post" action="{{ route($route) }}" class="mb-2">
    @csrf
    <input hidden name="delete" value="{{ $training_id }}" required>
    <button type="submit" class="form-control btn btn-outline-dark">Удалить</button>
</form>