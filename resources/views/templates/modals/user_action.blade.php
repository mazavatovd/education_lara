<!-- Модальное окно -->
<div class="modal fade" id="modal_user_{{ $user->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" id="modal_user">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Пользователь <span><b>
                    {{ $user->surname }}
                    {{ $user->name }}
                    {{ $user->lastname }}</b></span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Закрыть"></button>
            </div>
            @include('templates.modals.modal_components.user_action_body')
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Закрыть</button>
                <button type="button" class="btn btn-secondary">Сохранить изменения</button>
            </div>
        </div>
    </div>
</div>