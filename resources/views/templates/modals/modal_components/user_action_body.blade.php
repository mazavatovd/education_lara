<div class="modal-body">
    <a class="btn btn-outline-secondary form-control text-lg-start"
       href="{{ route('user.page', ['uid' => $user->id]) }}">
        Посмотреть заполненные данные
    </a>
    <a class="btn btn-outline-secondary form-control text-lg-start mt-2"
       href="{{ route('user_training.page', ['uid' => $user->id]) }}">
        Посмотреть повышения преподавателя
    </a>
    <a class="btn btn-outline-secondary form-control text-lg-start mt-2"
       href="{{ route('user_header.page', ['uid' => $user->id]) }}">
        Посмотреть/Редактировать ИПРП
    </a>
    <a class="btn btn-outline-secondary form-control text-lg-start mt-2"
       href="{{ route('pdf', ['uid' => $user->id]) }}">
        <div>
            Печать данных (PDF)
        </div>
    </a>
    <a class="btn btn-outline-secondary form-control text-lg-start mt-2">
        <div id="switch_role{{ $user->id }}">
            Изменить роль
        </div>
    </a>
    <div id="roles_field{{ $user->id }}">
    </div>
</div>