<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>{{ $title }}</title>
</head>
<body style="font-size: 14px">
<div style="margin-bottom: 5ex">
    <div style="text-align: center">
        <p>
            Министерство науки и высшего образования Российской Федерации<br>
            федеральное государственное бюджетное образовательное учреждение высшего образования<br>
            <b>"Российский экономический университет им. Г.В. Плеханова"</b><br>
            Среднее профессиональное образование<br>
        </p>
        <hr style="margin-bottom: 1px">
        <b>Московский приборостроительный техникум</b>
        <hr style="margin-top: 3px; margin-bottom: 1px">
        <p style="margin-top: 1px; font-size: 10px">
            <small>(Наименование структурного подразделения СПО)</small>
        </p>
        <p>
            <strong>Индивидуальный план работы преподавателя</strong><br>
            <strong>на {{ $iprp_year  }} учебный год</strong>
        </p>
    </div>
    <div>
        <p>
            Цикловая методическая комиссия: <u>{{ $iprp_CMC  }}</u>
        </p>
        <p>
            Преподаватель: <u>{{ $iprp_teacher_name  }}</u>
        </p>
        <p>
            Дисциплина, МДК: <u>{{ $iprp_discipline  }}</u>
        </p>
        <p>
            Специальность: <u>{{ $iprp_speciality  }}</u>
        </p>
        <hr style="margin-bottom: 1px">
        <p style="margin-top: 1px; text-align: center; font-size: 10px">
            <small>(перечислить все дисциплины, МДК, практики по каждой специальности, согласно нагрузке)</small>
        </p>
    </div>
</div>

<h3 style="text-align: center"><b>Раздел 1. Организационно - педагогическая работа</b></h3>

<p>1.1 Личные данные преподавателя</p>
<table border="1" style="border-collapse: collapse;margin-top: 3px;width: 100ex">
    <thead>
    <tr id="1">
        <td style="padding: 6px">Фамилия, имя, отчество<br>
            преподавателя
        </td>
        <td style="padding: 10px;">{{ $surname }} {{ $name }} {{ $lastname }}</td>
    </tr>
    <tr id="2">
        <td style="padding: 6px">Дата рождения</td>
        <td style="padding: 6px">{{ $birth_date }}</td>
    </tr>
    <tr id="3">
        <td style="padding: 6px">Все занимаемые должности,<br>
            включая другие организаци<br>
            в текущем году
        </td>
        <td style="padding: 6px">{{ $job_title }}</td>
    </tr>
    <tr id="4">
        <td style="padding: 6px">Образование, где и когда<br>
            окончили (название учебного<br>
            заведения полностью)
        </td>
        <td style="padding: 6px">{{ $university_name }}{{ ' Год окончания: ' . $education_year }}</td>
    </tr>
    <tr id="5">
        <td style="padding: 6px">Специальность и квалификация<br>
            по диплому
        </td>
        <td style="padding: 6px">{{ $job_title }}</td>
    </tr>
    <tr id="6">
        <td style="padding: 6px">Кол-во полных лет работы</td>
        <td style="padding: 6px">{{ $work_year }}</td>
    </tr>
    <tr id="7">
        <td style="padding: 6px">Преподает дисциплину(ы)</td>
        <td style="padding: 6px">{{ $plan_discipline }}</td>
    </tr>
    <tr id="8">
        <td style="padding: 6px">Категория, дата окончания<br>
            категории
        </td>
        <td style="padding: 6px">{{ $plan_category }}{{ ' Дата окончания категории: ' . $category_date }}</td>
    </tr>
    <tr id="9">
        <td style="padding: 6px">Стаж работы общий/<br>
            педагогический (через дробь)
        </td>
        <td style="padding: 6px">{{ $experience }}</td>
    </tr>
    <tr id="10">
        <td style="padding: 6px">Место и время последнего<br>
            повышения квалификации
        </td>
        <td style="padding: 6px">{{ $training_place }}{{ ' Дата повышения: ' . $training_date }}</td>
    </tr>
    <tr id="11">
        <td style="padding: 6px">Звание или ученая степень (год<br>
            получения свидетельства)
        </td>
        <td style="padding: 6px">{{ $degree }}{{ ' Год: ' . $degree_year }}</td>
    </tr>
    </thead>
</table>
<p>1.2 Количество учебных часов по нагрузке</p>
<p>
    В первом семестре: {{ $hours_1 }}
</p>
<p>
    Во втором семестре: {{ $hours_2 }}
</p>

<h3 style="text-align: center"><b>Раздел 2. Учебно-методическая работа</b></h3>
<div>
<div style="margin-bottom: 10px">
    <p style="text-indent: 40px;">     2.1 Подготовка УМК (в т.ч. изданные в течение отчетного периода КТП,
        рабочая программа, ФОС, методички), соответствие образовательным
        программам, предложение и перспективный план заданий.</p>
    <u>{{ $prepare_ymk }}</u>
</div>
<div style="margin-bottom: 10px">
    <p style="text-indent: 40px;">     2.2 Внедрение эффективных методов преподавания, обобщение,
        обмен педагогическим опытом, использование инновационных технологий,
        взаимопосещения занятий коллег.</p>
    <u>{{ $introduction_of_effective_teaching_methods }}</u>
</div>
<div style="margin-bottom: 10px">
    <p style="text-indent: 40px;">     2.3 Междисциплинарные связи.</p>
    <u>{{ $interdisciplinary_connections }}</u>
</div>
<div style="margin-bottom: 10px">
    <p style="text-indent: 40px;">     2.4 Организация самостоятельной работы обучающихся в т.ч. выполнения и
        проверка домашних заданий.</p>
    <u>{{ $independent_work }}</u>
</div>
<div style="margin-bottom: 10px">
    <p style="text-indent: 40px;">     2.5 Проведение открытых мероприятий (темы докладов на заседаниях ЦМК,
        открытых уроков и мероприятий, уроков-экскурсий, прочее).</p>
    <u>{{ $open_events }}</u>
</div>
<div style="margin-bottom: 10px">
    <p style="text-indent: 40px;">     2.6 Проведение внеаудиторных мероприятий с обучающимися.</p>
    <u>{{ $extracurricular_activities }}</u>
</div>
<div style="margin-bottom: 10px">
    <p style="text-indent: 40px;">     2.7 Обеспечение дисциплин учебной литературой.</p>
    <u>{{ $educational_literature }}</u>
</div>
<div style="margin-bottom: 10px">
    <p style="text-indent: 40px;">     2.8 Внедрения новых форм и методов обучения, средств активизации
        познавательной деятельности обучающихся.</p>
    <u>{{ $new_forms }}</u>
</div>
<div style="margin-bottom: 10px">
    <p style="text-indent: 40px;">     2.9 Подготовка программных материалов для промежуточных аттестаций и
        иных установленных форм контроля (анализ контрольных материалов и
        сессий).</p>
    <u>{{ $prepare_materials }}</u>
</div>
<div style="margin-bottom: 10px">
    <p style="text-indent: 40px;">     2.10 Предложения по совершенствованию учебного процесса на
        перспективу.</p>
    <u>{{ $education_process }}</u>
</div>
<div style="margin-bottom: 10px">
    <p style="text-indent: 40px;">     2.11 Организация самостоятельной и научно-исследовательской деятельности
        обучающихся (указать мероприятия, например название и место проведение
        олимпиады).</p>
    <u>{{ $activity }}</u>
</div>
<div style="margin-bottom: 10px">
    <p style="text-indent: 40px;">     2.12 Статьи и публикации в учебном году.</p>
    <u>{{ $articles }}</u>
</div>
<div style="margin-bottom: 10px">
    <p style="text-indent: 40px;">     2.13 Прочие планируемые достижения в учебном году.</p>
    <u>{{ $achievements }}</u>
</div>
</div>
<div>
    <h3 style="text-align: center;margin-bottom: 4px"><b>Раздел 3. Повышение квалификации</b></h3>
    <p style="text-align: center;font-size: 10px;margin-top: 3px">Cведения о повышении квалификации преподавателя</p>
    @foreach($training as $tr)
        <p>Год повышения: {{ $tr->year }}</p>
    <table border="1" style="border-collapse: collapse;margin-top: 10px;width: 100ex">
        <thead>
        <tr>
            <th style="padding: 6px">
                Примерные формы повышени квалификации
            </th>
            <th style="padding: 6px">
                Наличие
            </th>
            <th style="padding: 6px">
                План
            </th>
        </tr>
        <tr>
            <th style="text-align: left;padding: 6px">
                Повышение квалификации
            </th>
            <td style="padding: 6px">
                {{ $tr->qualification_exists }}
            </td>
            <td style="padding: 6px">
                {{ $tr->qualification_plan }}
            </td>
        </tr>
        <tr>
            <th style="text-align: left;padding: 6px">
                Стажировка, аспирантура,<br>
                курсы, прочее
            </th>
            <td style="padding: 6px">
                {{ $tr->internship_exists }}
            </td>
            <td style="padding: 6px">
                {{ $tr->internship_plan }}
            </td>
        </tr>
        </thead>
    </table>
    @endforeach
</div>
<div>
    <p style=" margin-bottom: 20px;margin-top: 40px">Примечание: все данные об образовании, повышение квалификации, ученой степени и<br>
        звании должны подтверждаться ксерокопиями документов.<br>
        Мероприятия по воспитательной работе вносятся в отдельный план.
    </p>
    <p>
        подпись преподавателя _______
    </p>
    <p style="text-align: center">(Ф.И.О - полностью)</p>
    <p style="margin-bottom: 1px">Директор<br>
    <u><span>Московского приборостроительного техникума</span></u><span style="float: right"><u>А.В. Чурилов</u></span></p>
    <p style="margin-left: 15ex; font-size: 9px">
        <span><small>Наименование структурного подразделения (СПО)</small></span>
        <span style="margin-left: 40ex">подпись</span>
        <span style="float: right">ФИО</span>
    </p>

</div>


</body>
