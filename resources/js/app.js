require('./bootstrap');

import Vue from "vue";
import axios from 'axios';

const switch_role = new Vue({
    el: '#switch_role',
    data: {
        message: 'switch_role'
    },
    methods: {
        ShowRole: function () {
            axios.get('/roles').then(function (response) {
                // handle success
                console.log(response);
            })
                .catch(function (error) {
                    // handle error
                    console.log(error);
                })
        }
    }
});
